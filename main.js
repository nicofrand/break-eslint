class BaseApp extends React.Component {
    render() {
        return (
            <React.Fragment>
                <ErrorReporter>
                    <header>
                    </header>
    
                    <main>
                        <Route path="/:section/:subsection?/:currentAccountId" render={this.makeMenu} />
                    </main>
                </ErrorReporter>
            </React.Fragment>
        );
    };
}
